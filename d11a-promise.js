"use strict";

// On crée une promesse
const getRand = new Promise((resolve, reject) => {
  // Math.random() retourne un nombre entre 0 et 1
  // Pour cet exemple, un nombre choisi au hasard permet de tester le fonctionnement de la promesse
  // Soit elle réussit, soit elle échoue
  const num = Math.random() * 1000;
  if (num < 500) {
    resolve(num);
  } else {
    reject({err: ">= 500"});
  }
});


// On appelle la promesse
getRand
.then((result) =>{
  console.log('result', result);
})
.catch((error) => {
  console.log('err', error.err);
});


// Si on appelle une 2eme fois, on obtient le mème résultat
// Le résultat est sauvegardé
getRand
.then((result) =>{
  console.log('result', result);
})
.catch((error) => {
  console.log('err', error.err);
});