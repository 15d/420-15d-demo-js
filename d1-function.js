"use strict";

var name1 = 'Garneau';
var age = 50;
var isCool = true;

function user(userName, userAge, userIsCool) {
  return ('Name : ' + userName + ', age : ' + userAge +
    ' and is the user cool ? ' + userIsCool);
}

console.log("user:", user(name1, age, isCool));

// ========================================================

let userArrow = (userName, userAge, userIsCool) => {
  return ('Name : ' + userName + ', age : ' + userAge +
    ' and is the user cool ? ' + userIsCool);
};

console.log("userArrow:", userArrow(name1, age, isCool));

// ========================================================

// Si la fonction prend un seul paramètre, on peut omettre les parenthèses
let addToOne = a => {
  return a + 1;
};

console.log('addToOne:', addToOne(2)); // 3

// ========================================================

// Si la fonction utilise seulement un return, on peut utiliser 
// la version courte sur une ligne
let addToOneSimple = a => a + 1;

console.log('addToOneSimple:', addToOneSimple(2)); // 3

// ========================================================

// Si la fonction ne prend pas de paramètres, il faut mettre
// des parenthèses vides
let GiveMeRandom = () => Math.random();
// On peut également mettre un underscore _
// let GiveMeRandom = _ => Math.random();

console.log('GiveMeRandom:', GiveMeRandom()); // Random number

// ========================================================
// Les paramètres de la fonction peuvent prendre une valeur par défaut
function ajouter(a, b = 1) {
  return a + b;
}

let resultat = ajouter(3, 2); // 5
resultat = ajouter(5); // 6
console.log('resultat:', resultat)


// ========================================================
// Exemple avec function puis fonction fléchée 
var add = function (x) {
  return function (y) {
    return x + y;
  };
};
let add2 = add(2);
console.log('add2:', add2(1));

// ES6
const adder = x => y => x + y;

let add5 = adder(5);
console.log('add5:', add5(1));
console.log('adder:', adder(1)(2));