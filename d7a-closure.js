"use strict";

/**
 * Exemple de closure avec une fonction retournant une fonction qui incrémente une valeur à chaque fois qu'elle est appelée
 * 
 */
const myFunction = () => {
    let myValue = 2;
    const childFunction = () => {
        console.log("myValue:", myValue += 1);
    };
    return childFunction;
};

const result = myFunction();
result();
result();
result();


