"use strict";

var foo = "bar"; // globale, visible partout

function test() {
  console.log(foo); // "bar"
  if (foo == "bar"){
    var baz = "var local"; // visible dans la fonction
    let fez = "let local"; // visible dans le block if
    console.log('fez dans la portée du if:', fez);
  }
  console.log("baz:", baz); // "var local"
  // console.log(fez); // erreur
}

test();

console.log("foo:", foo); // "bar"
// console.log(baz); // erreur