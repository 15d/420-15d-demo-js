"use strict";

// Exemple de closure avec une fonction retournant une fonction qui incrémente une valeur à chaque fois qu'elle est appelée
function compteur() {
    let count = 0;
    return function () { // fonction anonyme
        return count++;
    };
}
let plusUn = compteur();
console.log(plusUn()); //0 
console.log(plusUn()); //1
console.log(plusUn()); //2



// Autre exemple
// Les fermetures peuvent être utilisées pour mémoriser les résultats de fonctions coûteuses et ainsi éviter de recalculer les mêmes résultats.
function memoize(callback) {
    const cache = {};
    return function(x) {
        console.log('cache', cache)
        if (cache[x]) {
            return cache[x];
        } else {12
            const resultat = callback(x);
            cache[x] = resultat;
            return resultat;
        }
    };
}

const carreMemoize = memoize(function(nb) {
    console.log('Calcul en cours...');
    return nb * nb; // On imagine que c'est une opération coûteuse
});

console.log(carreMemoize(4)); // Calcul en cours... 16
console.log(carreMemoize(5)); // Calcul en cours... 25
console.log(carreMemoize(4)); // 16 (utilise le cache)
console.log(carreMemoize(5)); // 25 (utilise le cache)
