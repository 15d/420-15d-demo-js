"use strict";

// On crée une promesse dans une fonction
const getRand = () => {
  return new Promise((resolve, reject) => {
    const num = Math.random() * 1000;

    if (num < 500) {
      resolve(num);
    } else {
      reject({err: ">= 500"});
    }
  });
};


getRand()
.then((result) =>{
  console.log('result', result);
})
.catch((error) => {
  console.log('err', error.err);
});


// Si on appelle une 2eme fois, on n'obtient pas le même résultat maintenant
// car on appelle une fonction à chaque fois
getRand()
.then((result) =>{
  console.log('result', result);
})
.catch((error) => {
  console.log('err', error.err);
});