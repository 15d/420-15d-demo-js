"use strict";

// Mettre une fonction dans une boucle faisant référence à une variable à portée externe n'est pas une bonne pratique et peut entraîner des erreurs.
// Voici une solution :

const tab = [];

function compteur() {
    for (var i = 0; i < 5; i++) {
        tab.push(
            // Ajout d'une fonction générée dynamiquement dans le tableau
            // en appelant une fonction qui génère une nouvelle fonction.
            // Note: L'indice i est passé par copie.
            generateFunction(i)
        );
    }
}

// function generateFunction(indice) {
//     return function(){
//         console.log('fonction indice i = ', indice);
//     };
// }

// même fonction mais avec fonction fléchée
let generateFunction = indice => () => console.log('fonction indice i = ', indice);


compteur();

tab.forEach( (uneFonctionDeTab) => {
    uneFonctionDeTab();
});
