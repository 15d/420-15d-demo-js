// - Définir une IIFE qui prend en paramètre deux nombres et renvoie leur somme

const somme = (function (a, b) {
  console.log('a, b:', a, b)
  return a + b;
})(2, 3);


console.log('somme:', somme)

