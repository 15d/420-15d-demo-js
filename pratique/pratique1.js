
// Écrivez une fonction classique qui prend un tableau de nombres en paramètre et retourne la somme de tous les nombres :
function somme(tableau) {
  let total = 0;
  for (let i = 0; i < tableau.length; i++) {
    total += tableau[i];
  }
  return total;
}
console.log('somme:', somme([1, 2, 3, 4]))

// Transformez cette fonction en une fonction fléchée en utilisant une boucle for...of :
const somme1 = (tableau) => {
  let total = 0;
  for (const nombre of tableau) {
    total += nombre;
  }
  return total;
}

// Testez votre fonction en appelant somme([1, 2, 3, 4]) :
console.log("somme1:", somme1([1, 2, 3, 4])); // Devrait afficher 10

// Écrivez une autre fonction classique qui prend un tableau de chaînes de caractères en paramètre et retourne le nombre de caractères total de toutes les chaînes de caractères :
function longueur(tableau) {
  let totalCaracteres = 0;
  for (let i = 0; i < tableau.length; i++) {
    totalCaracteres += tableau[i].length;
  }
  return totalCaracteres;
}
console.log('longueur:', longueur(["hello", "world"]))

// Transformez cette fonction en une fonction fléchée en utilisant une boucle for...of :
const longueur1 = (tableau) => {
  let totalCaracteres = 0;
  for (const chaine of tableau) {
    totalCaracteres += chaine.length;
  }
  return totalCaracteres;
}

// Testez votre fonction en appelant longueur(["hello", "world"]) :
console.log("longueur1", longueur1(["hello", "world"])); // Devrait afficher 10
