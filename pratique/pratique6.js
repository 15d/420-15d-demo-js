// Déclarer un objet "personne" avec trois propriétés (nom, age et adresse), dont la propriété "adresse" qui contient elle-même trois propriétés (rue, ville et province). Ensuite, vous devez utiliser l'affectation par décomposition pour assigner les propriétés "nom", "ville" et "province" de l'objet "personne" à trois variables distinctes.
// Affichez les variables dans la console

// Déclaration de l'objet "personne"
const personne = {
  nom: "Aminata",
  age: 30,
  adresse: {
    rue: "Rue du stade",
    ville: "Nouna",
    province: "Kossi"
  }
};

// Utilisation de l'affectation par décomposition
const { nom, adresse: { ville, province } } = personne;

// Affichage des variables dans la console
console.log("Nom:", nom);
console.log("Ville:", ville);
console.log("Province:", province);
