// Écrire une fonction trierTableaux qui prend en paramètre 2 tableaux d'entiers et qui retourne un seul tableau trié en ordre croissant. La fonction doit utiliser l'opérateur Spread.
// Tester la fonction avec les tableaux suivants : [4, 2, 1] et [3, 5, 6].

function trierTableaux(tableau1, tableau2) {
  // Utilisez l'opérateur Spread (...) pour fusionner les deux tableaux
  const tableauFusionne = [...tableau1, ...tableau2];

  // Utilisez la méthode sort() pour trier le tableau fusionné en ordre croissant
  const tableauTrie = tableauFusionne.sort((a, b) => a - b);

  return tableauTrie;
}

const tableau1 = [4, 2, 1];
const tableau2 = [3, 5, 6];

const tableauTriCroissant = trierTableaux(tableau1, tableau2);
console.log(tableauTriCroissant); // Affichera [1, 2, 3, 4, 5, 6]

// Écrire une fonction qui prend en entrée un nombre variable de chaînes de caractères, et qui renvoie la chaîne de caractères la plus longue parmi celles-ci.
// Tester la fonction en lui passant les paramètres suivants : 'chien', 'chat', 'oiseau', 'souris'

function trouverChaineLaPlusLongue(...chaines) {
  let chaineLaPlusLongue = '';

  for (let chaine of chaines) {
    if (chaine.length > chaineLaPlusLongue.length) {
      chaineLaPlusLongue = chaine;
    }
  }

  return chaineLaPlusLongue;
}

const chaineLongue = trouverChaineLaPlusLongue('chien', 'chat', 'oiseau', 'souris');
console.log(chaineLongue); // Affichera 'oiseau'
