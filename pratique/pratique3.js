'use strict';

// Écrire une fonction anonyme qui prend deux entiers en paramètre et renvoie :
// un nombre négatif si le premier entier est inférieur au deuxième,
// un nombre positif si le premier entier est supérieur au deuxième,
// zéro si les deux entiers sont égaux.
const comparerEntiers = function(a, b) {
return a - b;
};

comparerEntiers(5, 8); // -3

// Écrire une fonction trierTableau qui prend un tableau d'entiers en paramètre et une fonction de comparaison.
function trierTableau(tableau, fonctionComparaison) {
  return tableau.sort(fonctionComparaison);
}

// Utiliser la fonction trierTableau pour trier le tableau [5, 8, 1, 3, 9, 2] en ordre croissant.
const tableau = [5, 8, 1, 3, 9, 2];
trierTableau(tableau, comparerEntiers);
console.log("Tri croissant:", tableau); // [1, 2, 3, 5, 8, 9]

// Utiliser la fonction trierTableau, mais cette fois-ci pour trier le tableau [5, 8, 1, 3, 9, 2] en ordre décroissant. Pour cela, vous pouvez inverser les valeurs renvoyées par la fonction de comparaison.
trierTableau(tableau, function(a, b) {
  return b - a; // Inverser l'ordre de tri
});
console.log("Tri décroissant:", tableau); // [9, 8, 5, 3, 2, 1]