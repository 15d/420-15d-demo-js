// - Créez une fonction getCatFact() qui renvoie une Promise.
// - À l'intérieur de cette fonction, utilisez fetch pour récupérer les données de l'API (https://catfact.ninja/fact).
// - Utilisez json() pour convertir la réponse en un objet JSON.
// - Une fois les données converties, Afficher la phrase contenu dans le résultat à la console.

function getCatFact() {
  // Utilisez fetch pour récupérer les données de l'API Cat Fact
  return fetch('https://catfact.ninja/fact')
    .then((response) => {
      // Vérifiez si la requête a réussi (statut 200 OK)
      if (!response.ok) {
        // Lève une erreur et exécute le catch
        throw new Error('La requête a échoué avec le statut ' + response.status);
      }
      // Utilisez json() pour convertir la réponse en un objet JSON
      return response.json();
    })
    .then((data) => {
      // Dans data, il y a le retour de response.json()
      return data.fact; // Renvoyer la phrase si vous souhaitez utiliser la valeur ailleurs
    })
    .catch((error) => {
      console.error('Erreur:', error);
      throw error; // Vous pouvez choisir de rejeter la Promise en cas d'erreur
    });
}

// Appeler la fonction et gérer la Promise
getCatFact()
  .then((fact) => {
    console.log('Récupéré avec succès:', fact);
  })
  .catch((error) => {
    console.error('Erreur lors de la récupération:', error);
  });
