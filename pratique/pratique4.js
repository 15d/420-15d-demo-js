"use strict";

/**
 * Closure avec une fonction retournant une fonction qui incrémente une valeur à chaque fois qu'elle est appelée
 * @returns {Object} - un objet avec deux méthodes: plus et moins
 */
function compteur() {
    let count = 0;
    return {
      plus: () => {
        return ++count;
      },
      moins: () => {
        return --count;
      }
    }

}
let compte = compteur();
console.log("plus", compte.plus()); 
console.log("plus", compte.plus()); 
console.log("plus", compte.plus()); 
console.log("moins", compte.moins());
console.log("moins", compte.moins());

/**
 * plus et moins sont des méthodes de l'objet retourné par la fonction compteur. Elles sont encapsulées dans la fonction compteur et ont accès à la variable count.
 */