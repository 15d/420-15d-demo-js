"use strict";

console.log('nom', nom); // undefined
var nom = 'cegep'; 

// Appel d'une fonction avant sa déclaration
add(3, 2); // 5

/**
 * Additionne deux nombres
 * @param  {int} a nombre à additioner
 * @param  {int} b nombre à additioner
 * @return {int}   résultat de l'addition
 */
function add(a, b) {
  return a + b;
}


// addAnonyme(3, 2); // ReferenceError: Cannot access 'addAnonyme' before initialization
                     // addAnonyme n'existe pas encore !

/**
 * Additionne deux nombres
 * Fonction anonyme assignée à la variable addAnonyme
 * @param  {int} a nombre à additioner
 * @param  {int} b nombre à additioner
 * @returns {int}   résultat de l'addition
 */
let addAnonyme = function(a, b) {
  return a + b;
};

addAnonyme(3, 2); // 5

