"use strict";

// objet person
const person = {
  firstName: "John",
  lastName: "Doe",
  age: 48,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};
// la fonction reçoit person comme paramètre
// on utilise la décomposition pour extraire seulement la propriété firstName
const printName = ({firstName}) => {
  console.log('firstName', firstName);
};
printName(person);

// on peut aussi utiliser la décomposition pour extraire plusieurs propriétés
const {firstName, age} = person;
// on crée deux constantes firstName et age qui contiennent les valeurs des propriétés firstName et age de l'objet person
console.log('name, age : ', firstName, age);


// destructuring avec un tableau
const hobbies = ["Manger", "Dormir"];
const [hobbies1, hobbies2] = hobbies;
console.log('hobbies1, hobbies2 : ', hobbies1, hobbies2);



