"use strict";

console.log('this', this); // Voir différence dans le navigateur et avec Node

function test() {
  console.log('this 5', this);
}

test();

const person = {
  firstName: "John",
  lastName: "Doe",
  fullName : function() {
    console.log('this', this);
    return this.firstName + " " + this.lastName;
  }
};

const person1 = {
  firstName: "John",
  lastName: "Doe",
  fullName : () => {
    console.log('this person1', this);
    return this.firstName + " " + this.lastName;
  }
};

console.log('person', person.fullName()); // John Doe
console.log('person1', person1.fullName()); // undefined undefined