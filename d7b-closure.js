"use strict";

// initialisation d'un tableau vide
const tab = [];

// Déclaration d'une fonction compteur
// Cette fonction remplit le tableau tab avec 5 fonctions qui affichent la valeur de i
function compteur() {
    for (var i = 0; i < 5; i++) { // remplacer var par let 
        tab.push(function () { 
            console.log('fonction indice i = ', i);
        });
    }
}

// Appel de la fonction compteur
compteur();
// console.log('tab', tab)
// tab est un tableau qui contient maintenant 5 fonctions qui affichent la valeur de i

// // On appelle chaque fonction de tab
tab.forEach( (uneFonctionDeTab) => {
    uneFonctionDeTab();
});


// Ne fonctionne pas car au moment où les fonctions uneFonctionDeTab sont exécutées,
// i est égal à 5
// i est déclaré avec "var", donc elle appartient à la portée parente
// Quand une fonction est ajoutée dans le tableau avec tab.push(), elle conserve une référence à la variable i, mais pas sa valeur à ce moment précis.
// Une fois que la boucle est terminée, la variable i a la valeur 5 (le dernier état après la boucle). Ainsi, toutes les fonctions dans le tableau tab se réfèrent à cette même variable i, qui vaut toujours 5.

// Si on déclare i avec "let", cela règle le problème car la portée de "let" est limitée au bloc
// Le mot-clé "let" crée une nouvelle instance de "i" pour chaque itération (une portée de bloc), donc chaque fonction conservera la bonne valeur de "i".


// Autre exemple
for (var i = 1; i < 5; i++) {  // remplacer var par let
    setTimeout(() => console.log(i), 1000);  // 5 5 5 5 5
}