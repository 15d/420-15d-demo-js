"use strict";


// Rest opérator 

function logArgs(...args) {
   console.log(args); // [ 'cegep', 'Garneau', 'Québec' ] => Array
}


logArgs('cegep', "Garneau", "Québec");
// On peut ainsi passer n'importe quel nombre d'argument à la fonction

//**************************************************************************************

function argsToObject(keys, ...values) {
   let object = {};
   object[keys] = values; // values est un Array

   return object;
}

let liste = argsToObject('Étudiants', 'Gilles', 'Romane', 'Gaël', 'Salomé');
console.log('liste', liste); // { 'Étudiants': [ 'Gilles', 'Romane', 'Gaël', 'Salomé' ] }

//**************************************************************************************
// Spread operator

// Permet de cloner un tableau ou un objet
let arr1 = [1, 2, 3];
let arr2 = [...arr1]; // clone arr1

console.log('...arr1', ...arr1); // 1 2 3
console.log('...arr2', arr2); // [ 1, 2, 3 ]

//**************************************************************************************

// Permet de concaténer des tableaux

let étudiants = ['Romane', 'Gaël', 'Salomé'];
let profs = ['Gilles', 'Legolas'];

// je veux obtenir ['Profs', 'Gilles', 'Legolas', 'Éudiants', 'Romane', 'Gaël', 'Salomé']
let TousLeMonde = ['Profs', ...profs, 'Étudiants', ...étudiants];
console.log('TousLeMonde', TousLeMonde);

//**************************************************************************************

// Trouver le plus petit nombre dans un tableau
let arr3 = [5, 2, -5, 8];
console.log('Min : ', Math.min(...arr3));

