"use strict";

// Fonction immédiatement invoquée
(function() {
  console.log("Je suis dans la fonction 1");
})();

// ========================================================

// On peut passer des paramètres
(function(i, j) {
  let k = i + j;
  console.log("Je suis dans la fonction 2 =>", k);
})(2, 3);

// ========================================================

// Les variables déclarées ne "polluent" pas le scope
(function() {
  var foo = "bar";
  console.log("Je suis dans la fonction 3 =>", foo);
})();

//console.log(foo); // undefined
// ========================================================

let app = (function() {
  let id = 123;
  console.log("Je suis dans la fonction app =>", id);
  return id;
})();

// Il est possible d'appeler la fonction plusieurs fois
console.log('app', app); // 123