// Tests à faire dans la console du navigateur
// Non bloquant
function asyncForEach(i, cb) {
  setTimeout(cb, 0, i) 
}
for (let i=0; i<50000; i++){
  asyncForEach(i, function (j) {
    console.log('j: ', j)
  })
}
// Bien que le délai soit 0, le fait d'utiliser setTimeout avec un délai de 0 place chaque fonction de rappel (cb) dans la file d'attente d'événements. JavaScript n'exécute pas immédiatement les tâches dans cette file ; il laisse d'abord le code synchrone (la boucle for) se terminer avant de traiter les tâches asynchrones dans la file d'attente.

// Une fois que la boucle for a terminé d'ajouter toutes les tâches à la file d'attente, JavaScript commence à exécuter les tâches en attente (les fonctions de rappel dans asyncForEach) une par une.


// bloquant
for (let i=0;i<50000;i++){
  console.log(`test ${i}`)
}

// La boucle for est bloquante, elle doit terminer avant que le code suivant ne puisse être exécuté. Cela signifie que le code synchrone doit attendre que la boucle for se termine avant de continuer. 