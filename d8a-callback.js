"use strict";

// Exemple de callback synchrone

function carre(x) {
    return x * x;
}

function cube(x) {
    return x * x * x;
}

function appTransform(tab, callback) {
    for (let i = 0; i < tab.length; i++) {
        tab[i] = callback(tab[i]);
    }
}

let tabs = [2, 5, 6];

// Appelle de la fonction "carre" pour éléments du tableau
appTransform(tabs, carre);
console.log('carre', tabs);

// Appelle de la fonction "cube" pour éléments du tableau
appTransform(tabs, cube);
console.log('cube', tabs);

console.log('Terminé');

// Utilise map() pour simplifier l'écriture
tabs = [2, 5, 6];
let tabsCarre = tabs.map(carre);
console.log('tabsCarre', tabsCarre);

// Exemple pour utiliser seulement map()
let tabsCarreMap = tabs.map(x => x * x);
console.log('tabsCarreMap', tabsCarreMap)
